function [v1, lam1] = Power(A, x0, maxiter)

%----------Power Method-----------%
% The inputs are an nxn matrix A and intial guess x0.
% The method uses the l_2 norm to normalized the eigenvector estimates
% The method terminates when the max iteration has been reached. 

norm2 = norm(x0)
xold = x0./(norm(x0))             % Normalize x0 with an l_2 norm
ynew = A*xold                  % "un-normalized" x vector
xnew = ynew./(norm(ynew))
eig1 = xnew'*ynew
% pause

for k = 1:maxiter
    niter = k;
    ynew = A*xnew
    eig1 = xnew'*ynew
    xnew = ynew./(norm(ynew))
%     pause
end

lam1 = eig1;
v1 = xnew;


    
