function [P, nfeval, lambda] = steffensen(g, p0)

  P = [p0];
  nfeval = 0;
  
  err = [];
  ext = 0.65292;

  n = 0;

  not_done = true;
  while not_done
    i = n+1;  
    err(i,1) = abs(ext - p0);
    
    p1 = feval(g, p0);
    p2 = feval(g, p1);
    nfeval = nfeval + 2;
    p0 = p2 - (p2 - p1)^2/(p2 - 2*p1 + p0);

    
    
    P = [P; p0];

    n = n + 1;

    not_done = (abs(feval(g, p0) - p0) >= 1.0e-8) && (n < 100);
    
        for i = 2:n
            lambda(i,1) = err(i)/err(i-1);
        end
    
  end
