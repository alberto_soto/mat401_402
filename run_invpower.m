%----Driver file for Inverse Power Method-----%

A = [3 0 -1; 0 2 0; -1 0 2];
x0 = [0 1 1]';
shift = 4;
maxiter = 5;

[v, lam] = Inpower(A, x0, shift, maxiter)