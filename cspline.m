function [A, z, c] = cspline(f, x)
a = f;
h = 1;
n = size(a,2);

z = zeros((n-2), 1);
c = zeros(n,1);

% Read zeros into coefficient matrix A %
A = zeros((n-2), (n-2));

% Main diagonal of A%
for i=1:(n-2);
    for j = 1:(n-2);
        if i == j;
            A(i,j) = 2*(2*h);
        end;
    end;
end;

% Diagonals above and below main diagonal %
for k = 1:(n-2);
    for m = 1:(n-1);
        if k == m+1
            A(k,m) = 1;
            A(m,k) = 1;
        end;
    end;
end;

for i=1:n-2
    z(i) = (a(i+2) - a(i+1)) - (a(i+1)-a(i)); 
end

z = 3*z;

c = A\z;
c = [0 c(1) c(2) c(3) 0]';






        
        