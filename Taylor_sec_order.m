function [Approx_der,t]=Taylor_sec_order(a,alpha,h,tmax)

f = inline ('t*atan(y)','t','y');
df = inline ('atan(y)*(1+((t.^2)/(y.^2+1)))','t','y');


max = (tmax-a)/h;
w = zeros(max+1,1);
t = zeros(max+1,1);
w(1) = alpha;
t(1) = a;

for i=2:max+1
    w(i) = w(i-1) + h*f(t(i-1),w(i-1))+h^2*df(t(i-1),w(i-1))/2;
    t(i) = t(1) + (i-1)*h;
end

Approx_der = w;
t;