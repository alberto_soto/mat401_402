function [P, nfeval, lambda] = secant402(f, I)

  p0 = I(1);
  p1 = I(2);
  fp0 = feval(f, p0);
  fp1 = feval(f, p1);

  nfeval = 2;

  P = [];
  
  alpha = [];
  lambda = (n,1);
  
%   lambda = 0.754877544719978;

  n = 0;

  not_done = true;
  while not_done
    p2 = p1 - fp1 * (p1 - p0)/(fp1 - fp0);
    fp2 = feval(f, p2);
    nfeval = nfeval + 1;

    p0 = p1;
    fp0 = fp1;
    p1 = p2;
    fp1 = fp2;

    P = [P p2];

    n = n + 1;
    
    err(n,1) = abs(0-p2);
    
%     for i = 2:n
%         lambda(n,1) = err(n)/err(n-1);

%     for i = 2:n
%         alpha(n,1) = (log(err(n)) - log(lambda))/log(err(n-1));
        
        not_done = (abs(fp1) > 1.0e-8) && (n < 100);
        
    for i = 3:n
        lambda(i,1) = (P(i)-P(i-1))/(P(i-1)-P(i-2));
    end
  end
