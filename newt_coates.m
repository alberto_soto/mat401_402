function [I_m, I_t, I_s]=newt_coates(a,b,index_f)

% Numerical Integration, Newton-Coates Quadrature %

% Midpoint Rule %
x0_m = (a+b)/2;

I_m = (b-a)*f(x0_m,index_f);

% Trapezoid Rule %

x0_t = a;
x1_t = b;

I_t = 0.5*(x1_t - x0_t)*(f(x0_t, index_f)+f(x1_t, index_f));

% Simpson's Rule %

x0_s = a;
x1_s = (a+b)/2;
x2_s = b;

I_s = ((x2_s - x0_s)/6)*(f(x0_s, index_f) + 4*f(x1_s, index_f) + f(x2_s,index_f));

% This defines the integrand, didn't use inline functions for some reason
function f_value = f(x,index)

switch index
case 1
    f_value = sqrt(1+cos(x).^2);
case 2
f_value = 1 ./(1+x.^2);
end
