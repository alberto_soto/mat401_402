function [P, Ph, nfeval, lambda] = aitken(g, p0)

  p1 = feval(g, p0);
  p2 = feval(g, p1);
  nfeval = 2;
  P = [p0; p1];
  Ph = [];
  
  err = [];
  ext = 0.65292; 

  n = 0;

  not_done = true;
  while not_done
      i = n+1;
    ph = p2 - (p2 - p1)^2/(p2 - 2*p1 + p0);
    
    err(i,1) = abs(ext - ph); 
    
    P = [P; p2];
    Ph = [Ph; ph];

    p0 = p1;
    p1 = p2;
    p2 = feval(g, p1);
    nfeval = nfeval + 1;

    n = n + 1;

    not_done = (abs(feval(g, ph) - ph) > 1.0e-8) && (n < 100);
        
    for i = 2:n
        lambda(i,1) = err(i)/err(i-1);
    end
  end
