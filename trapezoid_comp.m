function [Int_T]=trapezoid_comp(a,b,n)

% f = inline('sqrt(1+cos(x).^2)');

f = inline('exp(-x^2)');
h = (b-a)/n;

% Compute the sum of the function values at the endpoints
sumend = f(a) + f(b);

% Initialize the "interior node" sum
sumin = 0;

%  Compute the sum of function values of the interior nodes.
for i=1:n-1
    sumin = sumin + f(a+i*h);
end
% Compute the integral by the composite trapezoid rule
    Int_T = h*(sumend + 2*sumin)/2;
