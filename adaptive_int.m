function [Int_AQ,fcount]=adaptive_int(f,a,b,eps)

% Intialize %
fa = f(a);
fc = f((a+b)/2);
fb = f(b);

[Int_AQ,k] = adapt1(f,a,b,eps,fa,fc,fb);
fcount = k+3;


function [Int_AQ, fcount] = adapt1(f,a,b,eps,fa,fc,fb)
c = (a+b)/2;
fd = f((a+c)/2);
fe = f((c+b)/2);
Q_ac = (c-a)*(fa+4*fd+fc)/6;
Q_cb = (b-c)*(fc+4*fe+fb)/6;
Q_ab = (b-a)*(fa+4*fc+fb)/6;

error_est = abs(Q_ac + Q_cb - Q_ab);
if error_est < 10*eps
    Int_AQ = Q_ac + Q_cb;
    fcount = 2;
else 
    [Q_ac,ka]=adapt1(f,a,c,eps/2,fa,fd,fc);
    [Q_cb,kb]=adapt1(f,c,b,eps/2,fc,fe,fb);
    Int_AQ = Q_ac + Q_cb;
    fcount = ka+kb+2;
end

