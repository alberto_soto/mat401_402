function [Approx_der,t]=Euler_method(f,a,alpha,h,tmax)


max = (tmax-a)/h;
w = zeros(max+1,1);
t = zeros(max+1,1);
w(1) = alpha;
t(1) = a;

for i=2:max+1
    w(i) = w(i-1) + h*f(t(i-1),w(i-1));
    t(i) = t(1) + (i-1)*h;
end

Approx_der = w;
t;