function [r, niter] = NR(f, J, x0, maxiter)

%------------------Newton's Method------------------------%

% Zero r of the nonlinear system of equations f(x) = 0.
% Here J is the Jacobian matrix of f and x0 is the initial
% approximation of the zero r.
% The second output parameter niter stands for the number
% of performed iterations

% Adapted from: http://www.math.siu.edu/matlab/tutorial5.pdf 


    xold = x0(:);
    dnew = linsolve(feval(J,xold), -feval(f,xold));
    xnew = xold + dnew;
   
for k=1:maxiter
    xold = xnew;
    niter = k;
    Fnew = feval(f, xold);
    Jnew = feval(J,xold);
    dnew = linsolve(Jnew, -Fnew);
    xnew = xold + dnew;
end
r = xnew;

