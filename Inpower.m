function [v, lam] = Inpower(A, x0, shift, maxiter)

%----------Inverse Power Method-----------%
% The inputs are an nxn matrix A and intial guess x0.
% The method uses the l_2 norm to normalized the eigenvector estimates
% The method terminates when the max iteration has been reached. 


[L,U,P] = lu(A-shift*eye(3));

z_mid = L\x0;
y_new = U\z_mid;
mu = x0'*y_new / (x0'*x0);
x_new = y_new / (norm(y_new));

for k = 1:maxiter
    iter = k;
    z_mid = L\x_new;
    y_new = U\z_mid;
    mu = x_new'*y_new;
    x_new = y_new / (norm(y_new));
    pause
end
    
v = x_new;
lam = (1/mu) + shift;
