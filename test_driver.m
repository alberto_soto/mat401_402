function [] = test_driver()

%   f = inline('cos(pi*x) - x');
%   fp = inline('-pi*sin(pi*x) - 1');
%   g = inline('cos(pi*x)');
  
%   f = inline('x*(1-cos(x))');
%   fp = inline('1+x*sin(x)-cos(x)');
%   g = inline('exp(-x^2)');

%%%----Math 290B---%%%
% check for pertubation method %%
  f = inline('x^3-(0.001)*x-1');
  fp = inline('3*x^2-0.001');

  I = [-1, 3];
  p0 = 1;
  mult = 1;

%   'bisection'
%   [P, nfeval] = bisection(f, I)

%   'false position'
%   [P, nfeval, err, errapprox, lambda1] = falseposition(f, I)

%   'newton'
  [P, nfeval, en1, err, lambda] = newton(f, fp, p0, mult)

%   'secant'
%   [P, nfeval, lambda] = secant(f, I)

%   'aitken'
%   [P, Ph, nfeval, lambda] = aitken(g, p0)
% 
%   'steffensen'
%   [P, nfeval, lambda] = steffensen(g, p0)
