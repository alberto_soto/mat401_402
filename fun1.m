function z = fun1(x)
z = zeros(2,1);
z(1) = x(1)*x(2)+1;
z(2) = x(1)^3 + x(2)^3;