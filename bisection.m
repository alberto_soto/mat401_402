function [P, nfeval] = bisection(f, I)

  a = I(1);
  b = I(2);
  fa = feval(f, a);
  nfeval = 1;
  P = [];

  n = 0;

  not_done = true;
  while not_done
    m = (a + b)/2;
    fm = feval(f, m);
    nfeval = nfeval + 1;

    if (sign(fa)*sign(fm) < 0)
      b = m;
      fb = fm;
    else
      a = m;
      fa = fm;
    end

    P = [P m];

    n = n + 1;

    not_done = (abs(fm) > 1.0e-8) && (n < 100);
  end
