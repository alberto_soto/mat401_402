function [P, nfeval, en1, err, lambda] = newton(f, fp, p0, mult)

  P = [p0];
  nfeval = 0;
  
  en1 = [1];
  err = [1];
  
%   alpha = [];
  lambda = [];
  m = [];


  n = 0;

  not_done = true;
  while not_done
        pn1 = p0; 
    fp0 = feval(f, p0);
    fpp0 = feval(fp, p0);
    p0 = p0 - mult*(fp0 / fpp0);
    nfeval = nfeval + 2;
        pn2 = p0;
     
        en1 = [en1; abs(pn1 - pn2)];
        err = [err; abs(0-p0)];
        

    P = [P; p0];

    n = n + 1;
    

    
    for i = 2:n
        lambda(n,1) = err(n)/err(n-1);
    

        not_done = (abs(fp0) > 1.0e-12) && (n < 250);
        
%     for i = 2:n
%         alpha(n,1) = (log(err(n)) - log(lam))/log(err(n-1)) 
        
%     for i = 3:n
%         lambda(i,1) = (P(i)-P(i-1))/(P(i-1)-P(i-2));
%         m(i,1) = 1/ (1-lambda(i));
%         alpha(n,1) = (log(err(n)) - log(lam))/log(err(n-1));
    end
  end
