%----Driver file for Power Method-----%

A = [2 0 -1; 0 2 0; -1 0 2];
x0 = [1 1 1]';
maxiter = 4;

[v1, lam1] = Power(A, x0, maxiter)