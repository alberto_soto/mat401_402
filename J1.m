function s = J1(x)
s = [x(2) x(1); 3*x(1)^2 3*x(2)^2];