function [P, nfeval, err, errapprox, lambda] = falseposition(f, I)

  a = I(1);
  b = I(2);
  fa = feval(f, a);
  fb = feval(f, b);
  nfeval = 2;
  P = [];
  err = [];
  errapprox = [];
  lambda = [];
  lambda1 = [];

  n = 0;

  not_done = true;
  while not_done
    p = b - fb*(b - a)/(fb - fa);
    fp = feval(f, p);
    nfeval = nfeval + 1;

    if (sign(fa)*sign(fp) < 0)
      b = p;
      fb = fp;
    else
      a = p;
      fa = fp;
    end

    P = [P; p];
    
    n = n + 1;
    
    err(n,1) = abs(0-p);
    
     not_done = (abs(fp) > 1.0e-8) && (n < 100);
    
%     for i = 2:n
%         lambda1(n,1) = err(n)/err(n-1);

     
        for i = 3:n
            lambda(i,1) = (P(i)-P(i-1))/(P(i-1)-P(i-2));
            errapprox(i,1) = abs(lambda(i)/(1-lambda(i))) * abs(P(i)-P(i-1));
        end
  end
