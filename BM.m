function [r, niter] = BM(f, J, x0, maxiter)

%------------------Broyden's Method------------------------%

% Zero r of the nonlinear system of equations f(x) = 0.
% Here J is the Jacobian matrix of f and x0 is the initial
% approximation of the zero r.
% The second output parameter niter stands for the number
% of performed iterations


    xold = x0(:);
    A0 = feval(J,xold);
    A0new = (1/-3)*-A0;            % A inverse for this particular problem
    Fold = feval(f, xold);         % F(x0)  
    dnew = (-A0new * Fold);        % d(0)  
    xnew = (xold + dnew);          % x1
    Fnew = feval(f, xnew);         % F(x1)
    yvec = Fnew - Fold;           % y vector needed in A inverse update
    Ay = A0new * yvec;
    A0new = A0new + ((dnew - Ay)*dnew'*A0new)/(dnew'*Ay);
   
for k = 1:maxiter
    xold = xnew;
    Fold = Fnew;                    % Set F(x1) to F(x0) 
    niter = k;
    dnew = (-A0new * Fold);          % Update d
    xnew = (xold + dnew);            % Update x vector
    Fnew = feval(f, xnew);           % Compute F(x1)
    yvec = Fnew - Fold;             % y vector for A inverse update
    Ay = A0new * yvec;
    A0new = A0new + ((dnew - Ay)*dnew'*A0new)/(dnew'*Ay);
end
r = xnew;

