function [Int_S]=simpsons_int(a,b)

f = inline('exp(-x^2)');
% Simpson's Rule %

x0_s = a;
x1_s = (a+b)/2;
x2_s = b;

I_s = ((x2_s - x0_s)/6)*(f(x0_s) + 4*f(x1_s) + f(x2_s));
Int_S = I_s; 