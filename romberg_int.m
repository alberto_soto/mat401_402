function[Int_Romberg, fun_evals]=romberg_int(f,a,b,eps)


max = 8;
count = 1;
R_row = zeros(max, max);

R_row(1,1) = trapezoid_comp(a,b,1);
fun_evals = 3;
error = 10;
sumodd= 0; 

while (count < max && error > eps)
    count = count + 1;
    k = count;
    n = 2^(k-1);
    h = (b-a)/n;
%     R_row(k,1) = trapezoid_comp(a,b,n);
    for i=1:2:(2^(k-1)-1)
        x(i) = a + i*h;
        f_vals(i) = f(x(i));
        fun_evals = fun_evals + size(x,1);
    end
    sumodd = sum(f_vals);
    R_row(k,1) = R_row(k-1,1)/2 + h*sumodd;
    for j=2:k
        R_row(k,j) = (4^(j-1)*R_row(k,j-1)-R_row(k-1,j-1))/((4^(j-1))-1);
    end
error = abs(R_row(k,k) - R_row(k-1,k-1))/n;
end 
Int_Romberg = R_row;
fun_evals;
